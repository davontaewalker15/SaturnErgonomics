﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SaturnErgonomics.Areas.Identity.Data;
using SaturnErgonomics.Models;

[assembly: HostingStartup(typeof(SaturnErgonomics.Areas.Identity.IdentityHostingStartup))]
namespace SaturnErgonomics.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                //services.AddDbContext<SaturnErgonomicsContext>(options =>
                //    options.UseSqlServer("Server=tcp:prototypetestserver.database.windows.net,1433;Initial Catalog=Prototype Database;Persist Security Info=False;User ID=djw0017;Password=Djdwa35393696!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));

                //services.AddIdentity<SaturnErgonomicsUser, IdentityRole>()
                //    .AddEntityFrameworkStores<SaturnErgonomicsContext>();

              //  services.AddIdentity<SaturnErgonomics, IdentityRole>().AddEntityFrameworkStores<SaturnErgonomicsContext>();


             //   services.AddScoped<IUserClaimsPrincipalFactory<SaturnErgonomicsUser>, AdditionalUserClaimsPrincipalFactory>();
             //   services.AddSingleton<IAuthorizationHandler, IsAdminHandler>();

                //services.AddAuthorization(options =>
                //{
                //    options.AddPolicy("IsAdmin", policyIsAdminRequirement =>
                //    {
                //        policyIsAdminRequirement.Requirements.Add(new IsAdminRequirement());
                //    });
                //});
            });
        }
    }
}