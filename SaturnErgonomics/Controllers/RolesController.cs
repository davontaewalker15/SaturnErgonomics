﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SaturnErgonomics.Areas.Identity.Data;
using SaturnErgonomics.Models.ViewModels;

namespace SaturnErgonomics.Controllers
{
    public class RolesController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<SaturnErgonomicsUser> _userManager;
        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<SaturnErgonomicsUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
        // GET: Roles
        public ActionResult Index()
        {

            List<UserRolesViewModel> viewModels = _roleManager.Roles.Select(role => new UserRolesViewModel()
            {
                RoleID = role.Id,
                RoleName = role.Name
            }).ToList();

            
            return View(viewModels);
        }

        public async Task<IActionResult> AddUserToRole(string userID, string roleID)
        {

            return RedirectToAction("Index");
        }

        // GET: Roles/Details/5
        public ActionResult Details(Guid id)
        {
            UserRolesViewModel viewModel;

            IdentityRole userRole = _roleManager.Roles.Where(role => role.Id == id.ToString()).FirstOrDefault();
            List<string> users = _userManager.GetUsersInRoleAsync(userRole.Name).Result.Select(user => user.FirstName + " " + user.LastName).ToList();
            viewModel = new UserRolesViewModel()
            {
                RoleID = userRole.Id,
                RoleName = userRole.Name,
                Users = users
            };
            return View(viewModel);
        }

        // GET: Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }

        // GET: Roles/Edit/5
        public ActionResult Edit(string id)
        {
            UserRolesViewModel viewModel;

            IdentityRole userRole = _roleManager.Roles.Where(role => role.Id == id.ToString()).FirstOrDefault();
            
            List<string> users = _userManager.GetUsersInRoleAsync(userRole.Name).Result.Select(user => user.FirstName + " " + user.LastName).ToList();
            viewModel = new UserRolesViewModel()
            {
                RoleID = userRole.Id,
                RoleName = userRole.Name,
                Users = users
            };
            return View(viewModel);
        }

        // POST: Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid id, IFormCollection collection)
        {
            try
            {

                IdentityRole userRole = _roleManager.Roles.Where(role => role.Id == id.ToString()).FirstOrDefault();
                List<string> users = _userManager.GetUsersInRoleAsync(userRole.Name).Result.Select(user => user.FirstName + " " + user.LastName).ToList();
                userRole.Name = collection["RoleName"];
                var result = _roleManager.UpdateAsync(userRole);
                var idResult = result.Result;
               
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Roles/Delete/5
        public ActionResult Delete(string id)
        {
            IdentityRole userRole = _roleManager.Roles.Where(role => role.Id == id.ToString()).FirstOrDefault();
            List<string> users = _userManager.GetUsersInRoleAsync(userRole.Name).Result.Select(user => user.FirstName + " " + user.LastName).ToList();
            UserRolesViewModel viewModel = new UserRolesViewModel()
            {
                RoleID = userRole.Id,
                RoleName = userRole.Name
            };
            return View(viewModel);
        }

        // POST: Roles/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, IFormCollection collection)
        {
            try
            {
                IdentityRole userRole = _roleManager.Roles.Where(role => role.Id == id.ToString()).FirstOrDefault();
                _roleManager.DeleteAsync(userRole);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}