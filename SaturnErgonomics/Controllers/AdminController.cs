﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SaturnErgonomics.Areas.Identity.Data;
using SaturnErgonomics.Models;
using SaturnErgonomics.Models.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SaturnErgonomics.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        //RoleManager<IdentityRole> _roleManager;
        //UserManager<SaturnErgonomicsUser> _userManager;
        //public AdminController(RoleManager<IdentityRole> roleManager, UserManager<SaturnErgonomicsUser> userManager)
        //{
        //    _roleManager = roleManager;
        //    _userManager = userManager;
        //}
        // GET: /<controller>/
        public ViewResult Index()
        {
            return View();
        }

        public ViewResult AddEntry()
        {
            return View();
        }

        [HttpPost]
        public ViewResult AddEntry(IFormCollection entryForm)
        {
            
            //try
            //{
            //    using (PrototypeDatabaseContext db = new PrototypeDatabaseContext())
            //    {
            //        TForceType forceType = new TForceType
            //        {
            //            VcForce = entryForm["torqueType"]
            //        };
            //        int forceTypeId;

            //        db.TForceType.Add(forceType); 
            //        db.SaveChanges();

            //        forceTypeId = db.TForceType.Last().IForceTypeId;
            //        TForce force = new TForce
            //        {
            //            IForceTypeId = forceTypeId,

            //            IForceType = forceType
            //        };
            //        THand hand = db.THand.Where(x => x.VcHand == entryForm["hand"]).FirstOrDefault();
            //        bool isDominant = true;
            //        if (entryForm["isDominant"] == "false")
            //        {
            //            isDominant = false;
            //        }
            //        THandedness handedness = new THandedness
            //        {
            //            IHandId = hand.IHandId,
            //            BIsDominant = isDominant,

            //            IHand = hand
            //        };
            //        TRotationAxis rotationAxis = db.TRotationAxis.Where(x => x.VcRotationAxis == entryForm["rotationAxis"]).FirstOrDefault();

            //        int rotationAxisId = db.TRotationAxis.Last().TiRotationAxisId;

            //        TObjectHandle objectHandle = new TObjectHandle
            //        {
            //            TiRotationAxisId = Convert.ToByte(rotationAxisId),
            //            VcObjectHandle = entryForm["objectHandle"],

            //            TiRotationAxis = rotationAxis
            //        };
            //        TPosture posture = new TPosture
            //        {
            //            VcPostureDetails = entryForm["posture"]
            //        };
            //        TProcedure procedure = new TProcedure
            //        {
            //            VcProcedure = entryForm["dataCollection"]
            //        };
            //        int forceId;
            //        int handednessId;
            //        int objectHandleId;
            //        int postureId;
            //        int procedureId;
            //        db.TForce.Add(force);
            //        db.THandedness.Add(handedness);
            //        db.TObjectHandle.Add(objectHandle);
            //        db.TPosture.Add(posture);
            //        db.TProcedure.Add(procedure);
            //        db.SaveChanges();

            //        forceId = db.TForce.Last().IForceId;
            //        handednessId = db.THandedness.Last().IHandednessId;
            //        objectHandleId = db.TObjectHandle.Last().IObjectHandleId;
            //        postureId = db.TPosture.Last().IPostureId;
            //        procedureId = db.TProcedure.Last().IProcedureId;

            //        TAction action = new TAction
            //        {
            //            IForceId = forceId,
            //            IHandednessId = handednessId,
            //            IObjectHandleId = objectHandleId,
            //            IPostureId = postureId,
            //            IProcedureId = procedureId,

            //            IForce = force,
            //            IHandedness = handedness,
            //            IObjectHandle = objectHandle,
            //            IPosture = posture,
            //            IProcedure = procedure
            //        };
            //        TStrengthData strengthData = new TStrengthData
            //        {
            //            FMeanMale = Convert.ToDouble(entryForm["maleMean"]),
            //            FMeanFemale = Convert.ToDouble(entryForm["femaleMean"]),
            //            FSdmale = Convert.ToDouble(entryForm["maleSD"]),
            //            FSdfemale = Convert.ToDouble(entryForm["femaleSD"])
            //        };
            //        int actionId;
            //        int strengthDataId;
            //        db.TAction.Add(action);
            //        db.TStrengthData.Add(strengthData);
            //        db.SaveChanges();

            //        actionId = db.TAction.Last().IActionId;
            //        strengthDataId = db.TStrengthData.Last().IStrengthDataId;
            //        int year = Convert.ToInt32(entryForm["year"]);
            //        DateTime studyYear = new DateTime(year, 1, 1);
            //        TStudy study = new TStudy
            //        {
            //            IActionId = actionId,
            //            IStrengthDataId = strengthDataId,
            //            DtStudyYear = studyYear,
            //            VcSource = entryForm["source"],
            //            BArchived = false,
            //            IStudyFileId = 0,

            //            IAction = action,
            //            IStrengthData = strengthData
            //        };
            //        TSubject subject = new TSubject
            //        {
            //            TiMaleSubjectCount = Convert.ToByte(entryForm["maleNumber"]),
            //            TiFemaleSubjectCount = Convert.ToByte(entryForm["femaleNumber"]),
            //            VcSubjectDescription = entryForm["subjectDescription"],
            //            TiSubjectAgeRangeEnd = Convert.ToByte(entryForm["ageMax"]),
            //            TiSubjectAgeRangeBegin = Convert.ToByte(entryForm["ageMin"])
            //        };
            //        int studyId;
            //        int subjectId;
            //            db.TStudy.Add(study);
            //            db.TSubject.Add(subject);
            //            db.SaveChanges();

            //            studyId = db.TStudy.Last().IStudyId;
            //            subjectId = db.TSubject.Last().ISubjectId;

            //        TStudySubject studySubject = new TStudySubject
            //        {
            //            IStudyId = studyId,
            //            ISubjectId = subjectId,

            //            ISubject = subject,
            //            IStudy = study
            //        };
            //            db.TStudySubject.Add(studySubject);
                     
            //            db.SaveChanges();

            //    }
            //} catch (Exception e)
            //{

            //}
            return View();
        }

        public IActionResult EditEntry(int ID)
        {
            //EntryModel model = new EntryModel();
            //using (PrototypeDatabaseContext db = new PrototypeDatabaseContext())
            //{
            //    TStudySubject studySubject = db.TStudySubject.Where(x => x.IStudySubjectId == ID).FirstOrDefault();
            //                    TSubject subject = db.TSubject.Where(x => x.ISubjectId == studySubject.ISubjectId).FirstOrDefault();
            //    model.maleNumber = subject.TiMaleSubjectCount;
            //    model.femaleNumber = subject.TiFemaleSubjectCount;
            //    model.ageMax = subject.TiSubjectAgeRangeEnd;
            //    model.ageMin = subject.TiSubjectAgeRangeBegin;
            //    model.subjectDescription = subject.VcSubjectDescription;

            //    TStudy study = db.TStudy.Where(x => x.IStudyId == studySubject.IStudyId).FirstOrDefault();
            //    DateTime studyYear = study.DtStudyYear;
            //    int year = studyYear.Year;
            //    model.year = year;
            //    model.source = study.VcSource;

            //    TAction action = db.TAction.Where(x => x.IActionId == study.IActionId).FirstOrDefault();

            //    TStrengthData strengthData = db.TStrengthData.Where(x => x.IStrengthDataId == study.IStrengthDataId).FirstOrDefault();
            //    model.femaleMean = strengthData.FMeanFemale;
            //    model.maleMean = strengthData.FMeanMale;
            //    model.femaleSD = strengthData.FSdfemale;
            //    model.maleSD = strengthData.FSdmale;

            //    TForce force = db.TForce.Where(x => x.IForceId == action.IForceId).FirstOrDefault();

            //    TPosture posture = db.TPosture.Where(x => x.IPostureId == action.IPostureId).FirstOrDefault();
            //    model.posture = posture.VcPostureDetails;

            //    THandedness handedness = db.THandedness.Where(x => x.IHandednessId == action.IHandednessId).FirstOrDefault();
            //    model.isDominant = handedness.BIsDominant;

            //    TObjectHandle objectHandle = db.TObjectHandle.Where(x => x.IObjectHandleId == action.IObjectHandleId).FirstOrDefault();
            //    model.objectHandle = objectHandle.VcObjectHandle;

            //    TProcedure procedure = db.TProcedure.Where(x => x.IProcedureId == action.IProcedureId).FirstOrDefault();
            //    model.dataCollection = procedure.VcProcedure;

            //    TForceType forceType = db.TForceType.Where(x => x.IForceTypeId == force.IForceTypeId).FirstOrDefault();
            //    model.torqueType = forceType.VcForce;

            //    THand hand = db.THand.Where(x => x.IHandId == handedness.IHandId).FirstOrDefault();
            //    model.hand = hand.VcHand;

            //    TRotationAxis rotationAxis = db.TRotationAxis.Where(x => x.TiRotationAxisId == objectHandle.TiRotationAxisId).FirstOrDefault();
            //    model.rotationAxis = rotationAxis.VcRotationAxis;

            //    model.id = ID;
            //}
            return View(/*model*/);
        }

        [HttpPost]
        public ActionResult EditEntry(EntryModel model)
        {

            //try
            //{
            //    using (PrototypeDatabaseContext db = new PrototypeDatabaseContext())
            //    {
            //        TStudySubject studySubject = db.TStudySubject.Where(x => x.IStudySubjectId == model.id).FirstOrDefault();

            //        TSubject subject = db.TSubject.Where(x => x.ISubjectId == studySubject.ISubjectId).FirstOrDefault();
            //        subject.TiMaleSubjectCount = (byte)model.maleNumber;
            //        subject.TiFemaleSubjectCount = (byte)model.femaleNumber;
            //        subject.TiSubjectAgeRangeEnd = (byte)model.ageMax;
            //        subject.TiSubjectAgeRangeBegin = (byte)model.ageMin;
            //        subject.VcSubjectDescription = model.subjectDescription;

            //        TStudy study = db.TStudy.Where(x => x.IStudyId == studySubject.IStudyId).FirstOrDefault();
            //        int year = model.year;
            //        DateTime studyYear = new DateTime(year, 1, 1);
            //        study.DtStudyYear = studyYear;
            //        study.VcSource = model.source;

            //        TAction action = db.TAction.Where(x => x.IActionId == study.IActionId).FirstOrDefault();

            //        TStrengthData strengthData = db.TStrengthData.Where(x => x.IStrengthDataId == study.IStrengthDataId).FirstOrDefault();
            //        strengthData.FMeanFemale = model.femaleMean;
            //        strengthData.FMeanMale = model.maleMean;
            //        strengthData.FSdfemale = model.femaleSD;
            //        strengthData.FSdmale = model.maleSD;

            //        TForce force = db.TForce.Where(x => x.IForceId == action.IForceId).FirstOrDefault();

            //        TPosture posture = db.TPosture.Where(x => x.IPostureId == action.IPostureId).FirstOrDefault();
            //        posture.VcPostureDetails = model.posture;

            //        THandedness handedness = db.THandedness.Where(x => x.IHandednessId == action.IHandednessId).FirstOrDefault();
            //        handedness.BIsDominant = model.isDominant;

            //        TObjectHandle objectHandle = db.TObjectHandle.Where(x => x.IObjectHandleId == action.IObjectHandleId).FirstOrDefault();
            //        objectHandle.VcObjectHandle = model.objectHandle;

            //        TProcedure procedure = db.TProcedure.Where(x => x.IProcedureId == action.IProcedureId).FirstOrDefault();
            //        procedure.VcProcedure = model.dataCollection;

            //        TForceType forceType = db.TForceType.Where(x => x.IForceTypeId == force.IForceTypeId).FirstOrDefault();
            //        forceType.VcForce = model.torqueType;

            //        THand hand = db.THand.Where(x => x.IHandId == handedness.IHandId).FirstOrDefault();
            //        hand.VcHand = model.hand;

            //        TRotationAxis rotationAxis = db.TRotationAxis.Where(x => x.TiRotationAxisId == objectHandle.TiRotationAxisId).FirstOrDefault();
            //        rotationAxis.VcRotationAxis = model.rotationAxis;

            //        db.TForceType.Update(forceType);
            //        db.TForce.Update(force);
            //        db.THand.Update(hand);
            //        db.THandedness.Update(handedness);
            //        db.TRotationAxis.Update(rotationAxis);
            //        db.TObjectHandle.Update(objectHandle);
            //        db.TPosture.Update(posture);
            //        db.TProcedure.Update(procedure);
            //        db.TAction.Update(action);
            //        db.TStrengthData.Update(strengthData);
            //        db.TStudy.Update(study);
            //        db.TSubject.Update(subject);
            //        db.TStudySubject.Update(studySubject);

            //        db.SaveChanges();

            //    }
            //}
            //catch (Exception e)
            //{

            //}
            return View();
        }

        public void DeleteEntry(int ID)
        {
            //using (PrototypeDatabaseContext db = new PrototypeDatabaseContext())
            //{
            //    TStudySubject studySubject = db.TStudySubject.Where(x => x.IStudySubjectId == ID).FirstOrDefault();
            //    TSubject subject = db.TSubject.Where(x => x.ISubjectId == studySubject.ISubjectId).FirstOrDefault();
            //    TStudy study = db.TStudy.Where(x => x.IStudyId == studySubject.IStudyId).FirstOrDefault();

            //    TAction action = db.TAction.Where(x => x.IActionId == study.IActionId).FirstOrDefault();

            //    TStrengthData strengthData = db.TStrengthData.Where(x => x.IStrengthDataId == study.IStrengthDataId).FirstOrDefault();

            //    TForce force = db.TForce.Where(x => x.IForceId == action.IForceId).FirstOrDefault();

            //    TPosture posture = db.TPosture.Where(x => x.IPostureId == action.IPostureId).FirstOrDefault();

            //    THandedness handedness = db.THandedness.Where(x => x.IHandednessId == action.IHandednessId).FirstOrDefault();

            //    TObjectHandle objectHandle = db.TObjectHandle.Where(x => x.IObjectHandleId == action.IObjectHandleId).FirstOrDefault();

            //    TProcedure procedure = db.TProcedure.Where(x => x.IProcedureId == action.IProcedureId).FirstOrDefault();

            //    TForceType forceType = db.TForceType.Where(x => x.IForceTypeId == force.IForceTypeId).FirstOrDefault();

            //    THand hand = db.THand.Where(x => x.IHandId == handedness.IHandId).FirstOrDefault();

            //    TRotationAxis rotationAxis = db.TRotationAxis.Where(x => x.TiRotationAxisId == objectHandle.TiRotationAxisId).FirstOrDefault();

            //    db.TForceType.Remove(forceType);
            //    db.TForce.Remove(force);
            //    db.THand.Remove(hand);
            //    db.THandedness.Remove(handedness);
            //    db.TRotationAxis.Remove(rotationAxis);
            //    db.TObjectHandle.Remove(objectHandle);
            //    db.TPosture.Remove(posture);
            //    db.TProcedure.Remove(procedure);
            //    db.TAction.Remove(action);
            //    db.TStrengthData.Remove(strengthData);
            //    db.TStudy.Remove(study);
            //    db.TSubject.Remove(subject);
            //    db.TStudySubject.Remove(studySubject);

            //    db.SaveChanges();
            //}
        }
    }
}
