﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TStudySubject
    {
        public int IStudyDataId { get; set; }
        public int IStudyId { get; set; }
        public int IStrengthDataId { get; set; }
    }
}
