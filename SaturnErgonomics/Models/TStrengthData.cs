﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TStrengthData
    {
        public TStrengthData()
        {
            TStudyData = new HashSet<TStudyData>();
        }

        public int IStrengthDataId { get; set; }
        public double FMeanMale { get; set; }
        public double FMeanFemale { get; set; }
        public double FSdmale { get; set; }
        public double FSdfemale { get; set; }
        public byte? TiMaleSubjectCount { get; set; }
        public byte? TiFemalSubjectCount { get; set; }
        public string VcSubjectDescription { get; set; }
        public int? TiMaleSubjectAgeBegin { get; set; }
        public int? TiMaleSubjectAgeEnd { get; set; }
        public int? TiFemaleSubjectAgeBegin { get; set; }
        public int? TiFemaleSubjectAgeEnd { get; set; }

        public virtual ICollection<TStudyData> TStudyData { get; set; }
    }
}
