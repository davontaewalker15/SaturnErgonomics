﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TSubject
    {
        public int ISubjectId { get; set; }
        public byte TiMaleSubjectCount { get; set; }
        public byte TiFemaleSubjectCount { get; set; }
        public string VcSubjectDescription { get; set; }
        public byte TiSubjectAgeRangeBegin { get; set; }
        public byte TiSubjectAgeRangeEnd { get; set; }
    }
}
