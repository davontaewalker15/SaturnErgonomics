﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TRotationDirection
    {
        public byte TiRotationDirectionId { get; set; }
        public string VcRotationDirection { get; set; }
    }
}
