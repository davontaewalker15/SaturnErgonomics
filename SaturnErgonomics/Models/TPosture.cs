﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TPosture
    {
        public TPosture()
        {
            TPosturePhoto = new HashSet<TPosturePhoto>();
        }

        public int IPostureId { get; set; }
        public string VcPostureDetails { get; set; }

        public virtual ICollection<TPosturePhoto> TPosturePhoto { get; set; }
    }
}
