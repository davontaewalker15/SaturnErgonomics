﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace SaturnErgonomics.Models.ViewModels
{
    public class UserRolesViewModel
    {
        public string RoleID { get; internal set; }
        public string RoleName { get; internal set; }
        public IEnumerable<string> Users { get; internal set; }
    }
}
