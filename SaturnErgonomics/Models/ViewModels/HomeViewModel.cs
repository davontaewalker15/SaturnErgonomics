﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SaturnErgonomics.Models.ViewModels
{
    public class HomeViewModel
    {
        public int HandleID { get; set; }
        public List<SelectListItem> Handles { get; set; }
    }
}
