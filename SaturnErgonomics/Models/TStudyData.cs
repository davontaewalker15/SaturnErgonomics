﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TStudyData
    {
        public int IStudyDataId { get; set; }
        public int IStudyId { get; set; }
        public int IStrengthDataId { get; set; }

        public virtual TStrengthData IStrengthData { get; set; }
        public virtual TStudy IStudy { get; set; }
    }
}
