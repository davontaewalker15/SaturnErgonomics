﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TForce
    {
        public int IForceId { get; set; }
        public int IForceTypeId { get; set; }

        public virtual TForceType IForceType { get; set; }
    }
}
