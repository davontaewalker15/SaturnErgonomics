﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TAction
    {
        public TAction()
        {
            TStudy = new HashSet<TStudy>();
        }

        public int IActionId { get; set; }
        public string VcPostureDescription { get; set; }
        public int? IHandednessId { get; set; }
        public int? TiRotationDirectionId { get; set; }
        public int? IObjectHandleId { get; set; }
        public string VcCharacteristicsAndOrientation { get; set; }

        public virtual THandedness IHandedness { get; set; }
        public virtual TObjectHandle IObjectHandle { get; set; }
        public virtual ICollection<TStudy> TStudy { get; set; }
    }
}
