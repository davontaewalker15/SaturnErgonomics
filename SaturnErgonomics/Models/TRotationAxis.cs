﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TRotationAxis
    {
        public byte TiRotationAxisId { get; set; }
        public string VcRotationAxis { get; set; }
    }
}
