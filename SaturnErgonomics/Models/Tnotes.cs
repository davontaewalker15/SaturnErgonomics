﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class Tnotes
    {
        public int InoteId { get; set; }
        public int IStudyId { get; set; }
        public string VcNote { get; set; }

        public virtual TStudy Inote { get; set; }
    }
}
