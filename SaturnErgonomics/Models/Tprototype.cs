﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class Tprototype
    {
        public int IprototypeId { get; set; }
        public string VcDataCollection { get; set; }
        public string VcPosture { get; set; }
        public string VcTorqueType { get; set; }
        public int IstudyYear { get; set; }
    }
}
