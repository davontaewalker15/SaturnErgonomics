﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TStudy
    {
        public TStudy()
        {
            TStudyData = new HashSet<TStudyData>();
        }

        public int IStudyId { get; set; }
        public int IActionId { get; set; }
        public string VcDataCollected { get; set; }
        public int? DtStudyYear { get; set; }
        public string VcNotes { get; set; }
        public string VcSource { get; set; }
        public int IStudyFileId { get; set; }
        public bool BArchived { get; set; }

        public virtual TAction IAction { get; set; }
        public virtual Tnotes Tnotes { get; set; }
        public virtual ICollection<TStudyData> TStudyData { get; set; }
    }
}
