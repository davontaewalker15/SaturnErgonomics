﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class THandedness
    {
        public THandedness()
        {
            TAction = new HashSet<TAction>();
        }

        public int IHandednessId { get; set; }
        public string VcHandedness { get; set; }

        public virtual ICollection<TAction> TAction { get; set; }
    }
}
