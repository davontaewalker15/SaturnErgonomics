﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TObjectHandle
    {
        public TObjectHandle()
        {
            TAction = new HashSet<TAction>();
        }

        public int IObjectHandleId { get; set; }
        public string VcObjectHandle { get; set; }

        public virtual ICollection<TAction> TAction { get; set; }
    }
}
