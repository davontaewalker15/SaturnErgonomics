﻿using System;
using System.Collections.Generic;

namespace SaturnErgonomics.Models
{
    public partial class TProcedure
    {
        public int IProcedureId { get; set; }
        public string VcProcedure { get; set; }
    }
}
